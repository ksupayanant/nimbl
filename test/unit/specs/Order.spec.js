import Vue from 'vue';
import Order from '../../../src/components/Order';

describe('Order.vue', function()
{

  it('should render correct contents for Company name', function()
  {
    const Constructor = Vue.extend(Order);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('#companyName').textContent)
      .to.equal('CPF Saraburi');
  });

  it('should render correct contents for address', function()
  {
    const Constructor = Vue.extend(Order);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('#address').textContent)
    .to.equal('High Way2, Kaeng Khoi Saraburi Thailand');
  });

  it('should render correct contents for phone', function()
  {
    const Constructor = Vue.extend(Order)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('#phone').textContent)
    .to.equal('+66 087 348 7934')
  });

  it('should render correct contents for choice',function()
  {
    const Constructor = Vue.extend(Order);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('#sellTypeSelection').textContent)
    .to.equal('Retailers Merchant');
  });

});
