import Vue from 'vue';
import SideBar from '../../../src/components/SideBar';

describe('SideBar.vue', function()
{

  it('should has class after click', function()
  {
    const Constructor = Vue.extend(SideBar);
    const vm = new Constructor().$mount();
    expect(SideBar.data().items[1] === 'Orders');
  });

  it('Size should be 9', function()
  {
    const Constructor = Vue.extend(SideBar);
    const vm = new Constructor().$mount();
    expect(SideBar.data().items.length == 9);
  });

});
